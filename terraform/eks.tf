

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

module "eks" {
  source = "terraform-aws-modules/eks/aws"

  cluster_name                    = "Amir-cluster"
  cluster_version                 = "1.21"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  vpc_id                          = module.vpc.vpc_id
  subnet_ids                      = module.vpc.private_subnets

  eks_managed_node_group_defaults = {
    instance_types         = ["t2.micro", "t2.medium"]
    vpc_security_group_ids = [aws_security_group.firewall.id]
  }

  eks_managed_node_groups = {
    default = {}
  }
}
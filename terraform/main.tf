

resource "aws_security_group" "firewall" {

  vpc_id = module.vpc.vpc_id

  ingress {
    description      = "http-web-access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "https-web-access"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Amir's firewall"
  }
}


resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  vpc_security_group_ids = [aws_security_group.firewall.id]
  user_data              = file("../bash_script/run_app.sh")
  subnet_id              = element(module.vpc.public_subnets, 0)
  tags = {
    Name = "Amir's Machine"
  }
}

resource "aws_route53_zone" "primary" {
  name = "amir-lev.pcmp.aws.tikal.io"
}


resource "aws_route53_record" "web_dns" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "web.amir-lev.pcmp.aws.tikal.io"
  type    = "A"
  ttl     = "30"

  records = [aws_eip.instance_elastic_ip.public_ip]
}


resource "aws_route53_record" "tikal_io" {
  zone_id = data.aws_route53_zone.pcmp.zone_id
  name    = "amir-lev.pcmp.aws.tikal.io"
  type    = "NS"
  ttl     = "30"

  records = [
    aws_route53_zone.primary.name_servers[0],
    aws_route53_zone.primary.name_servers[1],
    aws_route53_zone.primary.name_servers[2],
    aws_route53_zone.primary.name_servers[3],
  ] // or records = aws_route53_zone.primary.name_servers
}

resource "aws_eip" "instance_elastic_ip" {
  instance = aws_instance.web.id
  vpc      = true
}
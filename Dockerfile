FROM golang:alpine
WORKDIR /usr/app

COPY /app-for-testing/go.mod ./
COPY /app-for-testing/go.sum ./
RUN go mod download

COPY ./app-for-testing .
RUN go build -o amir-app

EXPOSE 80

CMD ["./amir-app"]
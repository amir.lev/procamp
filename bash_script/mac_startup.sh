#!/bin/bash

echo "hello and welcome to your Mac $USER. First lets install homebrew"

echo "installing homebrew"

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo "installing DevOps tools with homebrew"

brew install terraform
terraform -v
brew install terragrunt
terragrunt -v
brew install kubectl
kubectl --help
brew install helm
helm -h
brew install minikube
minikube
brew install git
git --version
brew install --cask docker
docker --version
brew install --cask visual-studio-code
code --verison
brew install argocd
argocd
brew cask install google-cloud-sdk

curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
sudo installer -pkg AWSCLIV2.pkg -target /
which aws

echo "Now creating some good old aliases"

cat > ~/.zshrc <<EOL
alias c="code ."
alias cl="clear"
alias hl="helm ls"
alias hd="helm dependency list"
alias dp="docker ps"
alias home="cd ~/Desktop"
alias kgp="kubectl get pods"
alias kgs="kubectl get services"
alias kdp="kubectl describe pod"
alias kl="kubectl logs"
alias kgd="kubectl get deploy"
alias kgi="kubectl get ingress"
alias di="docker images"
alias dpq="docker ps -qa"
alias devops="cd ~/Desktop/DevOps-Tikal"
alias gn="sudo shutdown -s"
alias sleep="sudo pmset -a disablesleep 0"
alias awake="sudo pmset -a disablesleep 1" 
alias editalias="nano ~/.zshrc"
alias trf="terraform fmt"
alias tra="terraform apply"
alias trap="terraform apply -auto-approve"
alias trp="terraform plan"
alias tri="terraform init"
alias trd="terraform destroy"
EOL
cat ~/.zshrc

echo "Great! Don't forget to restart your shell :)"
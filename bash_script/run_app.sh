#!/bin/bash

echo Installing docker,

apt update

apt -y upgrade
apt -y install docker.io

systemctl start docker
systemctl enable docker

echo adding user to docker group
usermod -aG docker ubuntu

newgrp docker
docker --version

docker run -itd -p 80:80 tikokito/procamp-app
package main

import (
	"fmt"
	"log"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", runApp).Methods("GET")
	fmt.Print("listen on port :80")
	log.Fatal(http.ListenAndServe("0.0.0.0:80", r))
}

func runApp(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./html/index.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadGateway)
	}
	tmpl.Execute(w, nil)
}
